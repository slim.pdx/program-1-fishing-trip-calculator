#include <cctype>
#include <iostream>
using namespace std;

/**************************************************
 * Course:      CS162                             *
 * Author:      Samuel Lim                        *
 * Program:     Fishing Trip Expense Calculator   *
 * Style Guide: LLVM style                        *
 * Description: This program will prompt the user *
 * with a series of questions and determine the   *
 * estimated costs in USD of a fishing trip based *
 * on the user's answers. The costs are specified *
 * in the Prog1.pdf specs document on PSU CS162's *
 * Canvas web page.                               *
 *************************************************/

void greet_user() {
    // Define user input var
    char user_input;

    // Output fun fish emoji
    cout << "|\\   \\\\\\\\__     o" << endl;
    cout << "| \\_/    o \\    o" << endl;
    cout << "> _   (( <_  oo  " << endl;
    cout << "| / \\__+___/      " << endl;
    cout << "|/     |/" << endl;
    // Greet user
    cout << "Hello! Welcome to the Fishing Trip Expense Calculator!\nYou will "
            "be "
            "asked several questions to determine the final cost of your "
            "fishing "
            "trip. Would you like to continue? (Y/N): ";
    cin >> user_input;
    // Check user input 'y' or 'n', continue or quit program based on condition
    if (toupper(user_input) == 'Y') {
        cout << "Great! Let's continue." << endl;
    } else if (toupper(user_input) == 'N') {
        cout << "Quitting program...";
        exit(0);
    }
}

bool check_residency() {
    // Define var we will store user input in
    char user_input;
    // Prompt user if they are oregon resident
    cout << "Are you an Oregon resident? (Y/N): ";
    cin >> user_input;
    // Check user input and return bool based on condition
    if (toupper(user_input) == 'Y') {
        cout << "Great! As an Oregon resident, you qualify for discounted "
                "rates."
             << endl;
        return true;
    } else {
        cout << "Thank you, as a non-Oregon resident you will pay standard "
                "rates."
             << endl;
        return false;
    }
}

int get_age() {
    // Define var age to store user input
    int age;
    cout << "Please enter your age as an integer: ";
    cin >> age;
    cout << "You are " << age << " years old." << endl;

    return age;
}

bool check_youth(int age) {
    if (age > 12 && age < 17) {
        // User qualifies as youth!
        cout << "Congratulations, you are a youth and are eligible for "
                "discounted "
                "rates!"
             << endl;
        return true;
    } else {
        // User does not qualify as youth.
        cout << "Unfortunately you are not a youth and do not qualify for "
                "discounted rates."
             << endl;
        return false;
    }
}

int get_pass_type(bool is_resident, bool is_youth) {
    int pass_type_int;  // raw user input
    int pass_type;      // what we will return

    // Check if youth, return pass type 5 (see enum declaration)
    if (is_youth) {
        cout << "As a youth, your fishing license is $5!" << endl;
        return 5;
    }

    cout << "============================" << endl;
    cout << "= Available Fishing Passes =" << endl;
    cout << "============================" << endl;

    cout << " - (0) Annual Pass\n  - (1) Single-Day Pass\n  "
            "- (2) Two-Day Pass\n  - (3) Three-Day Pass"
         << endl;
    cout << "Please input the integer correlating to the above pass you would "
            "like to purchase: ";
    cin >> pass_type_int;

    // Check if resident
    if (is_resident && pass_type_int == 0) {
        // User is resident, and resident_annual pass = 0 (see enum
        // decleration)
        cout << "You are a resident and have chosen an annual pass!" << endl;
        pass_type = 0;
    } else if (!is_resident && pass_type_int == 0) {
        cout << "You are NOT a resident and have chosen an annual pass!"
             << endl;
        pass_type = 1;
    } else if (pass_type_int == 1) {
        cout << "You have chosen a single-day pass!" << endl;
        pass_type = 2;  // single-day-pass
    } else if (pass_type_int == 2) {
        cout << "You have chosen a two-day pass!" << endl;
        pass_type = 3;  // two-day-pass
    } else {
        // any other input we will assume is 3 to satisfy compile errors
        cout << "You have chosen a three-day pass!" << endl;
        pass_type = 4;  // three-day-pass
    }
    return pass_type;
}

int get_addtl_pass_type(bool is_resident, bool is_youth) {
    char wants_pass;  // First we check if user actually wants another pass. if
                      // not, we can skip everything and return none pass_type
                      // which is 0
    int addtl_pass_type_int;  // raw user input
    int addtl_pass_type;      // return var

    cout << "Would you like to add an additional pass? (Y/N): ";
    cin >> wants_pass;
    if (wants_pass == 'N' || wants_pass == 'n') {
        // user does not want an additional pass, return 0 (which
        // correlates to "none" in the AdditionalPassTypeEnum)
        cout << "Thanks, we will not add an additional pass." << endl;
        return 0;
    }  // assume any other value inputted is "Y" or "y"

    // If user is youth
    if (is_youth) {
        cout << "=================" << endl;
        cout << "= Youth Pricing =" << endl;
        cout << "=================" << endl;

        // Print pricing menu for youth
        cout << "Additional Add-On Passes:" << endl;
        cout << " - (0) Youth Hunting AND Fishing Combined License: +$5\n"
                " - (1) Youth Hunting AND Shellfish AND Fishing Combined: "
                "+$50"
             << endl;
        cout << "Please input the integer correlating to the above pass "
                "you "
                "would like to purchase: ";
        cin >> addtl_pass_type_int;

        // Determine return value based on user input
        if (addtl_pass_type_int == 0) {
            return 5;  // defined in below enum declaration
        } else if (addtl_pass_type_int == 1) {
            return 6;  // defined in below enum declaration
        }
    }

    // If user is resident:
    if (is_resident) {
        cout << "=======================" << endl;
        cout << "= Residential Pricing =" << endl;
        cout << "=======================" << endl;

        // Print pricing for resident
        cout << "Additional Add-On Passes:" << endl;
        cout << " - (0) Hunting license: +$73\n"    // enum: 1
                " - (1) Shellfish license: +$10\n"  // enum: 2
                " - (2) Combined: +$83"             // enum: 4
             << endl;

        // Prompt user for pass type
        cout << "Please input the integer correlating to the above pass "
                "you would like to purchase: ";
        cin >> addtl_pass_type_int;

        // Determine return value based on user input
        if (addtl_pass_type_int == 0) {
            return 1;
        } else if (addtl_pass_type_int == 1) {
            return 2;
        } else if (addtl_pass_type_int == 2) {
            return 4;
        }
    }
    // user is NOT resident-- this will be executed if none of the
    // above conditionals are met
    cout << "===========================" << endl;
    cout << "= Non-Residential Pricing =" << endl;
    cout << "===========================" << endl;

    // Print pricing for non-resident
    cout << "Additional Add-On Passes (Non-Resident):" << endl;
    cout << " - (0) Shellfish: +$28" << endl;  // enum: 3
    cout << "Please input the integer correlating to the above pass you "
            "would like to purchase: ";
    cin >> addtl_pass_type_int;

    return 3;
}

int check_if_renting() {
    // var to store user input
    char is_renting;

    // ask user if they want to rent equipment
    cout << "Would you like to rent equipment? The cost is $70/day (Y/N): ";
    cin >> is_renting;

    if (is_renting == 'N' || is_renting == 'n') {
        return 0;  // user is not renting, return false (or 0)
    }

    // assume user is renting if we hit this block
    return 1;
}

int get_num_days_renting() {
    // var to store user input
    int num_days;

    cout << "Please input the # of days you would like to rent equipment for: ";
    cin >> num_days;

    cout << "You are renting equipment for: " << num_days << " days." << endl;
    return num_days;
}

double calculate_total(int pass_type, int addtl_pass_type, int num_days) {
    // Define constants Fixed prices based on specs
    // Fishing Passes
    const double resident_annual_pass = 44.0;
    const double non_resident_annual_pass = 110.50;
    const double single_day_pass = 23.0;
    const double two_day_pass = 42.0;
    const double three_day_pass = 59.50;
    const double youth_pass = 5.0;
    // Additional licenses
    const double hunting_pass = 73.0;
    const double resident_shellfish_pass = 10.0;
    const double non_resident_shellfish_pass = 28.0;
    const double combined_pass =
        83.0;  // might not need this, is hunting_pass + annual_pass better?
    const double youth_hunting_pass = 5.0;
    const double youth_combined_pass = 50.0;
    // Equipment rental (based on num_days)
    const double equipment_per_day = 70.0;

    // the total amount we output after calculations
    double total = 0.0;

    // add cost of pass to total based on pass_type
    switch (pass_type) {
        // resident annual
        case 0:
            total += resident_annual_pass;
        // non-resident annual
        case 1:
            total += non_resident_annual_pass;
        // single day
        case 2:
            total += single_day_pass;
        // two day
        case 3:
            total += two_day_pass;
        // three day
        case 4:
            total += three_day_pass;
        // youth pass
        case 5:
            total += youth_pass;
    }

    // add cost of additional passes to total based on addtl_pass_type
    switch (addtl_pass_type) {
        // NONE
        case 0:
            total += 0;
        // Hunting
        case 1:
            total += hunting_pass;
        case 2:
            total += resident_shellfish_pass;
        case 3:
            total += non_resident_shellfish_pass;
        case 4:
            total += combined_pass;
        case 5:
            total += youth_hunting_pass;
        case 6:
            total += youth_combined_pass;
    }

    // calculate equipment rental fee based on num_days
    total += (equipment_per_day * num_days);

    // return total!
    return total;
}

int main() {
    // Define 'continue' var (used to break out of loop, or continue using
    // program)
    bool continue_program = true;
    // var to store user input on whether or not to quit program
    char continue_program_input;

    while (continue_program == true) {
        // Define user input variables
        bool is_resident, is_youth, is_renting;
        int age, num_days;
        // These are of ENUM type, which means they have a set amount of
        // 'options' int pass_type; // temporary until we cast into enum
        enum PassTypeEnum {
            ResidentAnnual = 0,
            NonResidentAnnual = 1,
            OneDay = 2,
            TwoDay = 3,
            ThreeDay = 4,
            Youth = 5
        };
        // additional_license type "combined" has constraint that user MUST
        // be resident
        enum AdditionalPassTypeEnum {
            None = 0,  // User does not want any additional pass
            Hunting = 1,
            Resident_shellfish = 2,
            NonResidentShellfish = 3,
            Combined = 4,
            YouthHunting = 5,
            YouthHuntingShellfish = 6
        };
        // Final total to output to user after receiving all inputs and
        // calculating
        double total = 0.0;
        // Declare pass_type and addtl_pass_type vars using above enums
        PassTypeEnum pass_type;
        AdditionalPassTypeEnum addtl_pass_type;

        /********************************
         * FINISH VARIABLE DECLARTIONS  *
         * START MAIN PROGRAM FLOW      *
         *******************************/

        // Execute function greet_user
        greet_user();
        // Ask if user is resident, set is_resident bool
        is_resident = check_residency();
        // Ask user age
        age = get_age();
        // Check if user qualifies as youth
        is_youth = check_youth(age);
        // Ask user what type of pass they want
        // On return, we typecast the int from get_pass_type to our custom
        // Enum
        // TODO: Write function to map enum values to strings, eg.
        // PassTypeEnum(0)
        // -> "Resident Annual Pass"
        pass_type =
            static_cast<PassTypeEnum>(get_pass_type(is_resident, is_youth));
        // Prompt user for additional pass
        addtl_pass_type = static_cast<AdditionalPassTypeEnum>(
            get_addtl_pass_type(is_resident, is_youth));
        // Prompt user if they would like to rent equipment
        is_renting = check_if_renting();
        // If renting, ask how many days
        if (is_renting) {
            num_days = get_num_days_renting();
        } else {  // if not renting set num_days to 0
            num_days = 0;
        }
        // Calculate total expenses based on variables
        total = calculate_total(pass_type, addtl_pass_type, num_days);
        // Output total
        cout << "\n [+] Your total cost for your fishing trip: $" << total
             << endl;

        // Ask user if they would like to run the program again or quit
        cout << "\nWould you like to run this program again? (Y/N): ";
        cin >> continue_program_input;
        // set continue_program bool based on above
        if (continue_program_input == 'N' || continue_program_input == 'n') {
            continue_program = false;
            break;
        } else {
            continue_program = true;
        }
    }

    return 0;
}

