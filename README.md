# Program 1 - Fishing Trip Expense Calculator

## Building binary

Run the command from the root directory: `make compile`

## Running binary

Run the command after building binary: `make run`

## How to build (manual)

1. Navigate to root dir: `cd ~/CS162/programs/program_1`
2. Compile program to `out/` directory: `g++ src/main.cpp -o ./out/main`

## Run Program (manual)

1. Navigate to output directory: `cd out`
2. Run executable: `./main`
3. Interact with program via REPL interface

