# Usage
# make		# compile binary to out/ dir
# make run	# compile binary AND run binary after compilation
# make test	# compile test binary and run it

all: help

help:
	@echo "Usage: make <FUNC>\n\n"
	@echo "Functions:\n- compile (compiles program to out/ directory\n- run(runs binary)\n- clean (removes contents out out/ directory)"

compile:
	@echo "Compiling main program..."
	# Check if out/ dir exists, if not create it
	mkdir -p ./out
	g++ src/main.cpp -o out/main
	@echo "Done!"

run:
	@echo "Running program..."
	# add check to see if compiled binary exists
	# if it doesn't, return exception and quit
	cd out/ && ./main

clean:
	@echo "Cleaning compiled binaries..."
	rm out/*
	@echo "Done!"
